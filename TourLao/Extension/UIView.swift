//
//  UIView.swift
//  TourLao
//
//  Created by Aira on 1/20/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func roundCorner(radius:CGFloat) {
        self.layer.cornerRadius = radius
//        self.layer.masksToBounds = true;
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 3.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
//        self.layer.masksToBounds = false
//        self.layer.cornerRadius = radius
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
