//
//  MDCTextFieldController.swift
//  TourLao
//
//  Created by Aira on 1/20/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation
import MaterialComponents

extension MDCTextInputControllerOutlined{
    func setMDCTextInputEmail(){
        self.activeColor = UIColor.blue///underline active color
        self.normalColor = UIColor.blue///underline initial color
        self.inlinePlaceholderColor = UIColor.blue
        self.floatingPlaceholderNormalColor = UIColor.blue
        self.floatingPlaceholderActiveColor = UIColor(named: "colorMainBlue")
    }
}
