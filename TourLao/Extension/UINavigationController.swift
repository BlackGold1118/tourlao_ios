//
//  UINavigationController.swift
//  TourLao
//
//  Created by Aira on 1/28/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation

extension UINavigationController {
   open override var preferredStatusBarStyle: UIStatusBarStyle {
      return topViewController?.preferredStatusBarStyle ?? .default
   }
}
