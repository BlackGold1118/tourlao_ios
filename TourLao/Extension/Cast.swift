//
//  Cast.swift
//  TourLao
//
//  Created by Aira on 1/26/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation

extension Double {

    func truncate(places: Int) -> Double {
        return Double(ceil(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }

}

extension Int {

    func percentage(outOf totalNumber: Int) -> Double {
        return ((Double(self)/Double(totalNumber)) * 100).truncate(places: 1)
    }

}
