//
//  Global.swift
//  TourLao
//
//  Created by Aira on 1/21/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Kingfisher

class Global: NSObject {
    
    static var gUser: User = User()
    static var gComment: [Comment] = [Comment]()
    static var gUserArr: [User] = [User]()
    
    static var urlEtl: String = "http://www.etllao.com/"
    static var urlPlaza: String = "http://laoplazahotel.com/"
    static var urlUnitel: String = "https://laotel.com/"
    
    static var gProvince: [Province] = [Province(id: "0", name: "Vientiane", description: "Vientiane, Laos' national capital, mixes French-colonial architecture with Buddhist temples such as the golden, 16th-century Pha That Luang, which is a national symbol.", img: 0, lat: 17.975706, log: 102.633104), Province(id: "1", name: "Nam Ha National Proected Area", description: "Nam Ha National Protected Area is a national protected area in Luang Namtha Province in northern Laos. This mostly forested park is home to a variety of ethnic groups and diverse animal and plant species. The park is an ecotourism destination.", img: 0, lat: 0.0, log: 0.0), Province(id: "2", name: "Northern", description: "Northern Laos.", img: 0, lat: 0.0, log: 0.0), Province(id: "3", name: "Luang Prabang", description: "Luang Prabang, the ancient capital of Luang Prabang Province in northern Laos, lies in a valley at the confluence of the Mekong and Nam Khan rivers.", img: 0, lat: 19.883396, log: 102.134687), Province(id: "4", name: "Phouhin Poun", description: "The Phou Hin Poun National Biodiversity Conservation Area, formerly known as the Khammouane Limestone National Biodiversity Conservation Area, is one of 21 National Biodiversity Conservation Areas of the Lao People's Democratic Republic.", img: 0, lat: 0.0, log: 0.0), Province(id: "5", name: "Southern Laos", description: "Southern Laos.", img: 0, lat: 0.0, log: 0.0), Province(id: "6", name: "Pakse", description: "Pakse or Pakxe is the capital and most populous city in the southern province of Champasak, making it the second most populous city in Laos. Located at the confluence of the Xe Don and Mekong Rivers, it has a population of about 88,000.", img: 0, lat: 15.110507, log: 105.817288),]
    
    static func onAnimationUIView(view: UIView, moveSpace value: CGFloat, direction isX: Bool = false, during time: Double = 0.3) {
        UIView.animate(withDuration: time, animations: {
            if isX {
                view.frame.origin.x += value
            } else {
                view.frame.origin.y += value
            }
        }, completion: nil)
    }
    
    static func emailValidation(email: String)->Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    static func passwordValidation(password: String)->Bool {
        if (password.count >= 6) {
            return true
        } else{
            return false
        }
    }
    
    static func onShowProgressView (name: String) {
        SVProgressHUD.show(withStatus: name)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setForegroundColor (UIColor.lightGray)
        SVProgressHUD.setBackgroundColor (UIColor.black.withAlphaComponent(0.0))
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setRingNoTextRadius(20)
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
    }
    
    static func onhideProgressView() {
        SVProgressHUD.dismiss()
    }
    
    static func loadImage(url: String, imageView: UIImageView) {
        let processor = DownsamplingImageProcessor(size: imageView.frame.size)
            |> RoundCornerImageProcessor(cornerRadius: 20)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: URL(string: url),
//            placeholder: UIImage(named: "img_girl"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}
