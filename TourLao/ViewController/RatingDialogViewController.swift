//
//  RatingDialogViewController.swift
//  TourLao
//
//  Created by Aira on 1/25/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import Cosmos

protocol RatingDialogDelegate {
    func onClickSubmit(rating: Double, comment: String)
}

class RatingDialogViewController: UIViewController {

    @IBOutlet var mainUV: UIView!
    @IBOutlet weak var dialogUV: UIView!
    @IBOutlet weak var starUV: CosmosView!
    @IBOutlet weak var submitUV: UIButton!
    @IBOutlet weak var commentTF: UITextField!
    
    var isUp = false
    var delegate: RatingDialogDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUIView()
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func initUIView() {
        dialogUV.roundCorner(radius: 7.0)
        submitUV.roundCorner(radius: 7.0)
        commentTF.delegate = self
        
        let tapMainUV = UITapGestureRecognizer(target: self, action: #selector(self.onTapMainUV(_:)))
        tapMainUV.delegate = self
        mainUV.addGestureRecognizer(tapMainUV)
    }
    
    @objc func onTapMainUV(_ sender: UITapGestureRecognizer) {
        if isUp {
            Global.onAnimationUIView(view: mainUV, moveSpace: 80.0)
            isUp = false
            commentTF.resignFirstResponder()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func onClickSubmitUB(_ sender: Any) {
//        if starUV.rating == 0.0 || commentTF.text!.isEmpty {
//            self.view.makeToast("No comment text and rating..")
//        } else {
//            self.dismiss(animated: true, completion: nil)
//        }
        self.delegate?.onClickSubmit(rating: starUV.rating, comment: commentTF.text!)
    }
}

extension RatingDialogViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == commentTF {
            Global.onAnimationUIView(view: self.mainUV, moveSpace: 80.0)
            self.isUp = false
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == commentTF {
            Global.onAnimationUIView(view: self.mainUV, moveSpace: -80.0)
            self.isUp = true
        }
    }
}

extension RatingDialogViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}

