//
//  DetailViewController.swift
//  TourLao
//
//  Created by Aira on 1/24/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import LIHImageSlider
import ImageSlideshow
import Cosmos
import CodableFirebase
import FirebaseDatabase
import MapKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var descriptionUL: UILabel!
    @IBOutlet weak var locationUL: UILabel!
    @IBOutlet weak var locationDescriptionUL: UILabel!
    @IBOutlet weak var mobliePhoneUL: UILabel!
    @IBOutlet weak var officePhoneUL: UILabel!
    @IBOutlet weak var emailUL: UILabel!
    @IBOutlet weak var priceUl: UILabel!
    @IBOutlet weak var avatar1UIMG: UIImageView!
    @IBOutlet weak var name1UL: UILabel!
    @IBOutlet weak var star1RatingCV: CosmosView!
    @IBOutlet weak var country1UL: UILabel!
    @IBOutlet weak var comment1UL: UILabel!
    @IBOutlet weak var avatar2UIMG: UIImageView!
    @IBOutlet weak var star2RatingCV: CosmosView!
    @IBOutlet weak var name2UL: UILabel!
    @IBOutlet weak var country2UL: UILabel!
    @IBOutlet weak var comment2UL: UILabel!
    @IBOutlet weak var ratingChartUV: TEABarChart!
    @IBOutlet weak var averageRatingUL: UILabel!
    @IBOutlet weak var totalCommentCount: UILabel!
    @IBOutlet weak var fiveStarUL: UILabel!
    @IBOutlet weak var fourStarUL: UILabel!
    @IBOutlet weak var threeStarUL: UILabel!
    @IBOutlet weak var twoStarUL: UILabel!
    @IBOutlet weak var oneStarUL: UILabel!
    @IBOutlet weak var moreUB: UIButton!
    @IBOutlet weak var avatar1UV: UIView!
    @IBOutlet weak var avatar2UV: UIView!
    @IBOutlet weak var chartUV: UIView!
    
    var product: Product?
    var commentArr: [Comment] = [Comment]()
    
    fileprivate var sliderVC: LIHSliderViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        initNavigationBar()
        initAnimationUIView()
        Database.database().reference().child("Comments").queryOrdered(byChild: "date").observe(.value, with: {snapshot in
            self.commentArr.removeAll()
            Global.gComment.removeAll()
            for comment in snapshot.children {
                let snap = comment as! DataSnapshot
                guard let value = snap.value else {
                    return
                }
                do {
                    let model = try FirebaseDecoder().decode(Comment.self, from: value)
                    if model.postid == self.product!.id {
                        self.commentArr.append(model)
                    }
                } catch let error {
                    print(error)
                }
            }
            self.commentArr.reverse()
            Global.gComment = self.commentArr
            self.initUIView()
            self.initChartUV()
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func initNavigationBar() {
        self.navigationItem.title = product?.name
    }
    
    func initChartUV() {
        let barChartColors = [
            UIColor.green,
            UIColor.blue,
            UIColor.red,
            UIColor.yellow,
            UIColor.gray
        ]
        
        var oneStarRatingCount = 0
        var twoStarRatingCount = 0
        var threeStarRatingCount = 0
        var fourStarRatingCount = 0
        var fiveStarRatingCount = 0
        var totalStarRating = 0
        
        totalCommentCount.text = "\(commentArr.count)"
        
        for item in commentArr {
            totalStarRating += item.rate
            switch item.rate {
            case 1:
                oneStarRatingCount += 1
                break
            case 2:
                twoStarRatingCount += 1
                break
            case 3:
                threeStarRatingCount += 1
                break
            case 4:
                fourStarRatingCount += 1
                break
            case 5:
                fiveStarRatingCount += 1
                break
            default:
                print("0")
                break
            }
        }
        
        ratingChartUV.barColors = barChartColors
        ratingChartUV.data = [fiveStarRatingCount, fourStarRatingCount, threeStarRatingCount, twoStarRatingCount, oneStarRatingCount]
        ratingChartUV.transform = CGAffineTransform(rotationAngle: .pi/2.0)
        averageRatingUL.text = "\((Double(totalStarRating) / Double(commentArr.count)).truncate(places: 1))"
        fiveStarUL.text = "\(fiveStarRatingCount) (\(fiveStarRatingCount.percentage(outOf: commentArr.count)))%"
        fourStarUL.text = "\(fourStarRatingCount) (\(fourStarRatingCount.percentage(outOf: commentArr.count)))%"
        threeStarUL.text = "\(threeStarRatingCount) (\(threeStarRatingCount.percentage(outOf: commentArr.count)))%"
        twoStarUL.text = "\(twoStarRatingCount) (\(twoStarRatingCount.percentage(outOf: commentArr.count)))%"
        oneStarUL.text = "\(oneStarRatingCount) (\(oneStarRatingCount.percentage(outOf: commentArr.count)))%"
        ratingChartUV.barSpacing = 3
        ratingChartUV.backgroundColor = UIColor.white
    }
    
    func initAnimationUIView() {
        var imageSource = [KingfisherSource]()
        for item in product!.photo {
            imageSource.append(KingfisherSource(urlString: item)!)
        }
        slideShow.slideshowInterval = 2.0
        slideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.blue
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        slideShow.pageIndicator = pageControl

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideShow.activityIndicator = DefaultActivityIndicator()
        slideShow.setImageInputs(imageSource)
    }
    
    func initUIView() {
        let provinceIndex = Int(product!.province)
        descriptionUL.text = product?.detail
        locationUL.text = Global.gProvince[provinceIndex!].name
        locationDescriptionUL.text = Global.gProvince[provinceIndex!].description
        mobliePhoneUL.text = product?.phone1
        officePhoneUL.text = product?.phone2
        emailUL.text = product?.email
        priceUl.text = product!.price + " Kip"
        
        let tapLocation = UITapGestureRecognizer(target: self, action: #selector(onTapLocation))
        locationUL.addGestureRecognizer(tapLocation)
        
        let tapMobilePhone = UITapGestureRecognizer(target: self, action: #selector(onTapMobilePhone))
        mobliePhoneUL.addGestureRecognizer(tapMobilePhone)
        
        let tapOfficePhone = UITapGestureRecognizer(target: self, action: #selector(onTapOfficePhone))
        officePhoneUL.addGestureRecognizer(tapOfficePhone)
        
        switch commentArr.count {
        case 0:
            avatar1UV.isHidden = true
            avatar2UV.isHidden = true
            moreUB.isHidden = true
            chartUV.isHidden = true
            break
        case 1:
            avatar1UV.isHidden = false
            avatar2UV.isHidden = true
            moreUB.isHidden = true
            chartUV.isHidden = false
            avatar1UVSetting(userid: commentArr[0].userid)
            break
        case 2:
            avatar1UV.isHidden = false
            avatar2UV.isHidden = false
            moreUB.isHidden = true
            chartUV.isHidden = false
            avatar1UVSetting(userid: commentArr[0].userid)
            avatar2UVSetting(userid: commentArr[1].userid)
            break
        default:
            avatar1UV.isHidden = false
            avatar2UV.isHidden = false
            moreUB.isHidden = false
            chartUV.isHidden = false
            avatar1UVSetting(userid: commentArr[0].userid)
            avatar2UVSetting(userid: commentArr[1].userid)
            break
        }
        
    }
    @IBAction func onClickBakcItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCommentItem(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RatingDialogViewController") as! RatingDialogViewController
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func onClickShareItem(_ sender: Any) {
        let shareLine = product?.name
        let firstActivityItem = "Share \(shareLine ?? "")"
        let secondActivityItem : NSURL = NSURL(string: "https://apps.apple.com/us/app/id")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func avatar1UVSetting(userid: String) {
        for user in Global.gUserArr {
            if user.id == userid {
                Global.loadImage(url: user.photo, imageView: avatar1UIMG)
                name1UL.text = user.name
                comment1UL.text = commentArr[0].content
                country1UL.text = user.country
                star1RatingCV.rating = Double(commentArr[0].rate)
            }
        }
    }
    
    func avatar2UVSetting(userid: String) {
        for user in Global.gUserArr {
            if user.id == userid {
                Global.loadImage(url: user.photo, imageView: avatar2UIMG)
                name2UL.text = user.name
                comment2UL.text = commentArr[1].content
                country2UL.text = user.country
                star2RatingCV.rating = Double(commentArr[1].rate)
            }
        }
    }
    
    @objc func onTapLocation() {
        var lat: Double = 0.0
        var lon: Double = 0.0
        for province in Global.gProvince {
            if province.id == self.product?.province {
                lat = province.lat
                lon = province.log
            }
        }
        
        makeMapCall(lat: lat, lon: lon)
        
    }
    
    @objc func onTapMobilePhone() {
        makePhoneCall(phoneNumber: mobliePhoneUL.text!.replacingOccurrences(of: " ", with: ""))
    }
    
    @objc func onTapOfficePhone() {
        makePhoneCall(phoneNumber: officePhoneUL.text!.replacingOccurrences(of: " ", with: ""))
    }
    
    func makePhoneCall(phoneNumber: String) {
        if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
            let alert = UIAlertController(title: ("Call " + phoneNumber + "?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                UIApplication.shared.open(phoneURL as URL, options: [:], completionHandler: nil)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeMapCall(lat: Double, lon: Double){
        let latitude:CLLocationDegrees =  lat
        let longitude:CLLocationDegrees =  lon

        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.locationUL.text
        mapItem.openInMaps(launchOptions: options)
    }
    
}

extension DetailViewController: RatingDialogDelegate {
    func onClickSubmit(rating: Double, comment: String) {
        if rating == 0.0 || comment.isEmpty{
            self.view.makeToast("No comment text and rating.")
        } else {
            guard let key = Database.database().reference().child("Comments").childByAutoId().key else {
                return
            }
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy - MM - dd"
            let strDate = formatter.string(from: date)
            let mComment: Comment = Comment(id: key, commentid: "", postid: self.product!.id, userid: Global.gUser.id, content: comment, date: strDate, rate: Int(rating))
            let data = try! FirebaseEncoder().encode(mComment)
            Database.database().reference().child("Comments").child(key).setValue(data){ (error, ref) -> Void in
                if error == nil{
                    self.initChartUV()
                    self.initUIView()
                }
                else {
                    print(error.debugDescription)
                }
            }
            dismiss(animated: false, completion: nil)
        }
    }
}

