//
//  ProfileViewController.swift
//  TourLao
//
//  Created by Aira on 1/22/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import MaterialComponents
import CountryPickerView
import ALCameraViewController
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var avatarUIMG: UIImageView!
    @IBOutlet weak var cameraBGUV: UIView!
    @IBOutlet weak var cameraUIMG: UIImageView!
    @IBOutlet weak var mdcNameTF: MDCTextField!
    @IBOutlet weak var mdcEmailTF: MDCTextField!
    @IBOutlet weak var mdcCountryTF: MDCTextField!
    @IBOutlet weak var mdcOldPasswordTF: MDCTextField!
    @IBOutlet weak var mdcNewPasswordTF: MDCTextField!
    @IBOutlet weak var mdcConfirmPasswordTF: MDCTextField!
    @IBOutlet weak var mdcFieldSV: UIStackView!
    @IBOutlet weak var updateUB: UIButton!
    @IBOutlet weak var postUV: MDCFloatingButton!
    
    var mdcNameController: MDCTextInputControllerOutlined?
    var mdcEmailController: MDCTextInputControllerOutlined?
    var mdcCountryController: MDCTextInputControllerOutlined?
    var mdcPasswordController: MDCTextInputControllerOutlined?
    var mdcNewPasswordController: MDCTextInputControllerOutlined?
    var mdcConfirmPasswordController: MDCTextInputControllerOutlined?
    var nameBtn: UIButton!
    var emailBtn: UIButton!
    var countryBtn: UIButton!
    var passowrdBtn: UIButton!
    var newBtn: UIButton!
    var confirmBtn: UIButton!
    
    var passwordToggle = false
    var confirmToggle = false
    var newToggle = false
    
    var minimumSize: CGSize = CGSize(width: 60, height: 60)
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: minimumSize)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        self.hideKeyboardWhenTappedAround()
        initNavigationBar()
        initMDCField()
        initUIView()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "Profile"
        self.navigationItem.backBarButtonItem?.title = ""
    }
    
    func initMDCField() {
        mdcNameController = MDCTextInputControllerOutlined(textInput: mdcNameTF)
        mdcEmailController = MDCTextInputControllerOutlined(textInput: mdcEmailTF)
        mdcCountryController = MDCTextInputControllerOutlined(textInput: mdcCountryTF)
        mdcPasswordController = MDCTextInputControllerOutlined(textInput: mdcOldPasswordTF)
        mdcNewPasswordController = MDCTextInputControllerOutlined(textInput: mdcNewPasswordTF)
        mdcConfirmPasswordController = MDCTextInputControllerOutlined(textInput: mdcConfirmPasswordTF)
        
        mdcNameController?.setMDCTextInputEmail()
        mdcEmailController?.setMDCTextInputEmail()
        mdcCountryController?.setMDCTextInputEmail()
        mdcPasswordController?.setMDCTextInputEmail()
        mdcNewPasswordController?.setMDCTextInputEmail()
        mdcConfirmPasswordController?.setMDCTextInputEmail()
        
        mdcNameTF.clearButton.isHidden = true
        mdcEmailTF.clearButton.isHidden = true
        mdcCountryTF.clearButton.isHidden = true
        mdcOldPasswordTF.clearButton.isHidden = true
        mdcNewPasswordTF.clearButton.isHidden = true
        mdcConfirmPasswordTF.clearButton.isHidden = true
        
        nameBtn = UIButton(type: .custom)
        nameBtn.setImage(UIImage(named: "ic_user"), for: .normal)
        nameBtn.tintColor = UIColor(named: "colorMainBlue")
        nameBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        nameBtn.frame = CGRect(x: CGFloat(mdcNameTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        mdcNameTF.rightView = nameBtn
        mdcNameTF.rightViewMode = .always
        
        emailBtn = UIButton(type: .custom)
        emailBtn.setImage(UIImage(named: "ic_mail"), for: .normal)
        emailBtn.tintColor = UIColor(named: "colorMainBlue")
        emailBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        emailBtn.frame = CGRect(x: CGFloat(mdcEmailTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        mdcEmailTF.rightView = emailBtn
        mdcEmailTF.rightViewMode = .always
        
        countryBtn = UIButton(type: .custom)
        countryBtn.setImage(UIImage(named: "ic_country"), for: .normal)
        countryBtn.tintColor = UIColor(named: "colorMainBlue")
        countryBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        countryBtn.frame = CGRect(x: CGFloat(mdcCountryTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        countryBtn.addTarget(self, action: #selector(showCountryPicker), for:   .touchUpInside)
        mdcCountryTF.rightView = countryBtn
        mdcCountryTF.rightViewMode = .always
        
        passowrdBtn = UIButton(type: .custom)
        passowrdBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        passowrdBtn.tintColor = UIColor(named: "colorMainBlue")
        passowrdBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        passowrdBtn.frame = CGRect(x: CGFloat(mdcOldPasswordTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        passowrdBtn.addTarget(self, action: #selector(onTapPasswordUB), for:    .touchUpInside)
        mdcOldPasswordTF.rightView = passowrdBtn
        mdcOldPasswordTF.rightViewMode = .always
        
        newBtn = UIButton(type: .custom)
        newBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        newBtn.tintColor = UIColor(named: "colorMainBlue")
        newBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        newBtn.frame = CGRect(x: CGFloat(mdcNewPasswordTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        newBtn.addTarget(self, action: #selector(onTapNewPasswordUB), for:    .touchUpInside)
        mdcNewPasswordTF.rightView = newBtn
        mdcNewPasswordTF.rightViewMode = .always
        
        confirmBtn = UIButton(type: .custom)
        confirmBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        confirmBtn.tintColor = UIColor(named: "colorMainBlue")
        confirmBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        confirmBtn.frame = CGRect(x: CGFloat(mdcConfirmPasswordTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        confirmBtn.addTarget(self, action: #selector(onTapConfirmPasswordUB), for:    .touchUpInside)
        mdcConfirmPasswordTF.rightView = confirmBtn
        mdcConfirmPasswordTF.rightViewMode = .always
        
        mdcNameTF.delegate = self
        mdcEmailTF.delegate = self
        mdcCountryTF.delegate = self
        mdcOldPasswordTF.delegate = self
        mdcNewPasswordTF.delegate = self
        mdcConfirmPasswordTF.delegate = self
    }
    
    func initUIView() {
        if Global.gUser.photo != "" {
            Global.loadImage(url: Global.gUser.photo, imageView: avatarUIMG)
        }
//        if Global.gUser.type == 0 {
//            postUV.isHidden = true
//        }
        postUV.isHidden = true
        
        mdcNameTF.text = Global.gUser.name
        mdcEmailTF.text = Global.gUser.email
        mdcCountryTF.text = Global.gUser.country
        avatarUIMG.roundCorners(corners: [.allCorners], radius: avatarUIMG.frame.size.height / 2)
        cameraBGUV.roundCorners(corners: [.allCorners], radius: cameraBGUV.frame.size.height / 2)
        
        updateUB.layer.cornerRadius = 10.0
        postUV.layer.cornerRadius = postUV.frame.size.height / 2
        postUV.backgroundColor = UIColor(named: "colorMainBlue")
        
        let tapCamera = UITapGestureRecognizer(target: self, action: #selector(onTapCameraUIMG))
        cameraUIMG.isUserInteractionEnabled = true
        cameraUIMG.addGestureRecognizer(tapCamera)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func showCountryPicker() {
        let alert = UIAlertController(title: "Select your language", message: "\n\n\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        let countryPickerUV = CountryPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 140))
        countryPickerUV.showPhoneCodeInView = false
        alert.view.addSubview(countryPickerUV)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.mdcCountryTF.text = countryPickerUV.selectedCountry.name
            print("You selected : \(countryPickerUV.selectedCountry.name)" )
        }))
        self.present(alert,animated: true, completion: nil )
    }
    
    @objc func onTapPasswordUB() {
        if !passwordToggle {
            passowrdBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcOldPasswordTF.isSecureTextEntry = false
        } else {
            passowrdBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcOldPasswordTF.isSecureTextEntry = true
        }
        passwordToggle = !passwordToggle
    }
    
    @objc func onTapNewPasswordUB() {
        if !newToggle {
            newBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcNewPasswordTF.isSecureTextEntry = false
        } else {
            newBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcNewPasswordTF.isSecureTextEntry = true
        }
        newToggle = !newToggle
    }
    
    @objc func onTapConfirmPasswordUB() {
        if !confirmToggle {
            confirmBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcConfirmPasswordTF.isSecureTextEntry = false
        } else {
            confirmBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcConfirmPasswordTF.isSecureTextEntry = true
        }
        confirmToggle = !confirmToggle
    }
    
    @objc func onTapCameraUIMG() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImageFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImageFromLibrary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickUpdateProfileUV(_ sender: Any) {
        if UserDefaults.standard.string(forKey: "type") == "FaceBook" {
            if Global.gUser.photo == "" {
                self.view.makeToast("Require your profile photo.")
                return
            }
            if mdcNameTF.text == "" {
                self.view.makeToast("Require your name.")
                return
            }
            if mdcCountryTF.text == "" {
                self.view.makeToast("Require your country.")
                return
            }
            else {
                Global.onShowProgressView(name: "Updating...")
                Global.gUser.name = self.mdcNameTF.text!
                 Global.gUser.country = self.mdcCountryTF.text!
                 var ref: DatabaseReference!
                ref = Database.database().reference()
                ref.child("Users").child(Global.gUser.id).setValue(Global.gUser.toFirebaseData())
                Global.onhideProgressView()
            }
        }
        else {
            if Global.gUser.photo == "" {
                self.view.makeToast("Require your profile photo.")
                return
            }
            if mdcNameTF.text == "" {
                self.view.makeToast("Require your name.")
                return
            }
            if mdcCountryTF.text == "" {
                self.view.makeToast("Require your country.")
                return
            }
            if mdcOldPasswordTF.text == "" {
                self.view.makeToast("Require your password.")
                return
            }
            if mdcOldPasswordTF.text!.count < 6 {
                self.view.makeToast("Require your 6 more digits password.")
                return
            }
            if mdcNewPasswordTF.text == "" {
                self.view.makeToast("Require your new password.")
                return
            }
            if mdcNewPasswordTF.text!.count < 6 {
                self.view.makeToast("Require your 6 more digits password.")
                return
            }
            if mdcConfirmPasswordTF.text == "" {
                self.view.makeToast("Require your confirm password.")
                return
            }
            if mdcNewPasswordTF.text != mdcConfirmPasswordTF.text {
                self.view.makeToast("Confirm your new password.")
                return
            }
            else{
                Global.onShowProgressView(name: "Updating...")
                Auth.auth().currentUser?.updatePassword(to: mdcNewPasswordTF.text!, completion: {error in
                    if error != nil {
                        Global.onhideProgressView()
                        self.view.makeToast("Your current password is not correct.")
                    }
                    else {
                        Global.gUser.name = self.mdcNameTF.text!
                        Global.gUser.country = self.mdcCountryTF.text!
                        var ref: DatabaseReference!
                       ref = Database.database().reference()
                   ref.child("Users").child(Global.gUser.id).setValue(Global.gUser.toFirebaseData())
                        Global.onhideProgressView()
                    }
                })
            }
        }
        
    }
    
    @IBAction func onClickPostUV(_ sender: Any) {
//        try? Auth.auth().signOut()
    }
    @IBAction func onClickBackItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSignOutItem(_ sender: Any) {
        try! Auth.auth().signOut()
        UserDefaults.standard.removeObject(forKey: "isRemeber")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject( forKey: "password")
        UserDefaults.standard.removeObject(forKey: "type")
        for vc in (self.navigationController?.viewControllers ?? []) {
            if vc is LoginViewController {
                let popVC = vc as! LoginViewController
                popVC.isCheckRemember = false
                popVC.checkRememberUIMG.image = UIImage(named: "ic_uncheck")
                popVC.mdcEmailTF.text = ""
                popVC.mdcPasswordTF.text = ""
                _ = self.navigationController?.popToViewController(popVC, animated: true)
                break
            }
        }
    }
    
    func getImageFromCamera() {
        let cameraViewController = CameraViewController(croppingParameters: croppingParameters, allowsLibraryAccess: true) { [weak self] image, asset in
            self?.avatarUIMG.image = image
            self!.uploadAvatarImage()
            self?.dismiss(animated: true, completion: nil)
        }
        present(cameraViewController, animated: true, completion: nil)
    }
    
    func getImageFromLibrary() {
        let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingParameters) { [weak self] image, asset in
            self?.avatarUIMG.image = image
            self!.uploadAvatarImage()
            self?.dismiss(animated: true, completion: nil)
        }
        present(libraryViewController, animated: true, completion: nil)
    }
    
    func uploadAvatarImage() {
        var data = NSData()
        data = self.avatarUIMG.image!.jpegData(compressionQuality: 0.3)! as NSData
        // path where you wanted to store img in storage
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let storageRef = Storage.storage().reference()
        let fileName = "avatar/\(UIDevice.current.identifierForVendor!.uuidString)\(Int64(Date().timeIntervalSince1970*1000))"
        storageRef.child(fileName).putData(data as Data, metadata: metaData){(metaData,error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }else{
                //store downloadURL
                storageRef.child(fileName).downloadURL(completion: {(url, error) in
                    if error != nil {
                        print(error?.localizedDescription)
                    }
                    else {
                        Global.gUser.photo = url!.absoluteString
                        print(Global.gUser.photo)
                    }
                })

            }
        }
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mdcEmailTF || textField == mdcCountryTF {
            return false
        }
        if UserDefaults.standard.string(forKey: "type") == "FaceBook" {
            if textField == mdcNewPasswordTF || textField == mdcOldPasswordTF || textField == mdcConfirmPasswordTF {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == mdcNameTF {
            self.mdcNameController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        if textField == mdcOldPasswordTF {
            self.mdcPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        if textField == mdcNewPasswordTF {
            self.mdcNewPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        if textField == mdcConfirmPasswordTF {
            self.mdcConfirmPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mdcNameTF {
            self.mdcNameController?.setErrorText("Please enter your name.", errorAccessibilityValue: nil)
        }
        if textField == mdcOldPasswordTF {
            self.mdcPasswordController?.setErrorText("Length must be over 6 digits.", errorAccessibilityValue: nil)
        }
        if textField == mdcNewPasswordTF {
            self.mdcNewPasswordController?.setErrorText("Length must be over 6 digits.", errorAccessibilityValue: nil)
        }
        if textField == mdcConfirmPasswordTF {
            self.mdcConfirmPasswordController?.setErrorText("Must be equal to new password.", errorAccessibilityValue: nil)
        }
    }
}

