//
//  RegisterViewController.swift
//  TourLao
//
//  Created by Aira on 1/21/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import MaterialComponents
import Toast_Swift
import FirebaseAuth
import FirebaseDatabase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var signupSV: UIStackView!
    @IBOutlet weak var mdcNameTF: MDCTextField!
    @IBOutlet weak var mdcEmailTF: MDCTextField!
    @IBOutlet weak var mdcPasswordTF: MDCTextField!
    @IBOutlet weak var mdcConfirmTF: MDCTextField!
    @IBOutlet weak var registerUV: UIButton!
    @IBOutlet weak var mainUV: UIView!
    
    var mdcNameController: MDCTextInputControllerOutlined?
    var mdcEmailController: MDCTextInputControllerOutlined?
    var mdcPasswordController: MDCTextInputControllerOutlined?
    var mdcConfirmController: MDCTextInputControllerOutlined?
    var nameBtn: UIButton!
    var emailBtn: UIButton!
    var passwordToggleBtn: UIButton!
    var confiremToggleBtn: UIButton!
    
    var passwordToggle = false
    var confirmToggle = false
    var isUp = false
    let valueUp: CGFloat = 100.0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
//        definesPresentationContext = true
        initNavigationBar()
        initMDCTextField()
        initUIView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "Register"
        self.navigationItem.backBarButtonItem?.title = ""
    }
    
    func initMDCTextField() {
        mdcNameController = MDCTextInputControllerOutlined(textInput: mdcNameTF)
        mdcEmailController = MDCTextInputControllerOutlined(textInput: mdcEmailTF)
        mdcPasswordController = MDCTextInputControllerOutlined(textInput: mdcPasswordTF)
         mdcConfirmController = MDCTextInputControllerOutlined(textInput: mdcConfirmTF)
        
        mdcNameController?.setMDCTextInputEmail()
        mdcEmailController?.setMDCTextInputEmail()
        mdcPasswordController?.setMDCTextInputEmail()
        mdcConfirmController?.setMDCTextInputEmail()
        
        mdcNameTF.clearButton.isHidden = true
        mdcEmailTF.clearButton.isHidden = true
        mdcPasswordTF.clearButton.isHidden = true
        mdcConfirmTF.clearButton.isHidden = true
        
        nameBtn = UIButton(type: .custom)
        nameBtn.setImage(UIImage(named: "ic_user"), for: .normal)
        nameBtn.tintColor = UIColor(named: "colorMainBlue")
        nameBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        nameBtn.frame = CGRect(x: CGFloat(mdcNameTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        mdcNameTF.rightView = nameBtn
        mdcNameTF.rightViewMode = .always
        
        emailBtn = UIButton(type: .custom)
        emailBtn.setImage(UIImage(named: "ic_mail"), for: .normal)
        emailBtn.tintColor = UIColor(named: "colorMainBlue")
        emailBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        emailBtn.frame = CGRect(x: CGFloat(mdcEmailTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        mdcEmailTF.rightView = emailBtn
        mdcEmailTF.rightViewMode = .always
        
        passwordToggleBtn = UIButton(type: .custom)
        passwordToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        passwordToggleBtn.tintColor = UIColor(named: "colorMainBlue")
        passwordToggleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        passwordToggleBtn.frame = CGRect(x: CGFloat(mdcPasswordTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        passwordToggleBtn.addTarget(self, action: #selector(self.refreshPassword), for: .touchUpInside)
        mdcPasswordTF.rightView = passwordToggleBtn
        mdcPasswordTF.rightViewMode = .always
        
        confiremToggleBtn = UIButton(type: .custom)
        confiremToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        confiremToggleBtn.tintColor = UIColor(named: "colorMainBlue")
        confiremToggleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        confiremToggleBtn.frame = CGRect(x: CGFloat(mdcConfirmTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        confiremToggleBtn.addTarget(self, action: #selector(self.refreshConfirm), for: .touchUpInside)
        mdcConfirmTF.rightView = confiremToggleBtn
        mdcConfirmTF.rightViewMode = .always
        
        mdcNameTF.delegate = self
        mdcEmailTF.delegate = self
        mdcPasswordTF.delegate = self
        mdcConfirmTF.delegate = self
        
    }
    
    func initUIView() {
        registerUV.roundCorner(radius: 7.0)
        
        let tapMainUV = UITapGestureRecognizer(target: self, action: #selector(onClickMainUV))
        mainUV.addGestureRecognizer(tapMainUV)
    }
    
    @objc func refreshPassword(){
        if !passwordToggle {
            passwordToggleBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcPasswordTF.isSecureTextEntry = false
        } else {
            passwordToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcPasswordTF.isSecureTextEntry = true
        }
        passwordToggle = !passwordToggle
        if isUp {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: valueUp)
            isUp = !isUp
        }
        mdcPasswordTF.resignFirstResponder()
    }
    
    @objc func refreshConfirm(){
        if !confirmToggle {
            confiremToggleBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcConfirmTF.isSecureTextEntry = false
        } else {
            confiremToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcConfirmTF.isSecureTextEntry = true
        }
        passwordToggle = !passwordToggle
        if isUp {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: valueUp)
            isUp = !isUp
        }
        mdcConfirmTF.resignFirstResponder()
    }
    
    @objc func onClickMainUV() {
        if isUp {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: valueUp)
            isUp = false
        }
        mdcNameTF.resignFirstResponder()
        mdcPasswordTF.resignFirstResponder()
        mdcEmailTF.resignFirstResponder()
        mdcConfirmTF.resignFirstResponder()
        
        mdcNameController?.setErrorText(nil, errorAccessibilityValue: nil)
        mdcEmailController?.setErrorText(nil, errorAccessibilityValue: nil)
        mdcPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
        mdcConfirmController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    @IBAction func onClickRegisterUV(_ sender: Any) {
        if mdcNameTF.text!.count > 0 && Global.emailValidation(email: mdcEmailTF.text!) && Global.passwordValidation(password: mdcPasswordTF.text!) && mdcPasswordTF.text! == mdcConfirmTF.text!{
            //Register Firebase
            Global.onShowProgressView(name: "Connecting..")
            Auth.auth().createUser(withEmail: mdcEmailTF.text!, password: mdcPasswordTF.text!, completion: {(user, error) in
                if let error = error{
                    print(error.localizedDescription)
                    Global.onhideProgressView()
                    self.view.makeToast("Email already Exists.")
                }
                else {
                    Global.onhideProgressView()
                    Global.gUser.id = (user?.user.uid)!
                    Global.gUser.name = self.mdcNameTF.text!
                    Global.gUser.email = self.mdcEmailTF.text!
                    Global.gUser.photo = ""
                    Global.gUser.country = ""
                    Global.gUser.type = 0
                    
                    var ref: DatabaseReference!
                    ref = Database.database().reference()
                    ref.child("Users").child(Global.gUser.id).setValue(Global.gUser.toFirebaseData())
                    
                    user?.user.sendEmailVerification(completion: {(error) in
                        if let error = error{
                            print(error.localizedDescription)
                            return
                        }
                        try? Auth.auth().signOut()
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            })
        }
        else {
            self.view.makeToast("Chekc your validation.")
        }
    }
    @IBAction func onClickBackItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onTapFlag() {
        
    }
}


extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == mdcConfirmTF {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: valueUp)
            isUp = false
            self.mdcConfirmController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        else if textField == mdcNameTF {
            self.mdcNameController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        else if textField == mdcEmailTF {
            self.mdcEmailController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        else if textField == mdcPasswordTF {
            self.mdcPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mdcConfirmTF {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: -valueUp)
            isUp = true
            self.mdcConfirmController?.setErrorText("Must be equal to Password.", errorAccessibilityValue: nil)
        }
        if ((textField == mdcEmailTF || textField == mdcNameTF || textField == mdcPasswordTF) && isUp == true) {
            Global.onAnimationUIView(view: self.signupSV, moveSpace: valueUp)
            isUp = false
        }
        if textField == mdcEmailTF {
            self.mdcEmailController?.setErrorText("Match example@gmail.com", errorAccessibilityValue: nil)
        }
        if textField == mdcPasswordTF {
            self.mdcPasswordController?.setErrorText("Length must be over 6 digits.", errorAccessibilityValue: nil)
        }
        if textField == mdcNameTF {
            self.mdcNameController?.setErrorText("Please enter your name.", errorAccessibilityValue: nil)
        }
    }
}
