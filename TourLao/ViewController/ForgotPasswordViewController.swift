//
//  ForgotPasswordViewController.swift
//  TourLao
//
//  Created by Aira on 1/27/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import MaterialComponents
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailMDCTF: MDCTextField!
    @IBOutlet weak var forgotUB: UIButton!
    @IBOutlet weak var bgUIMG: UIImageView!
    
    var nameBtn: UIButton!
    var mdcEmailController: MDCTextInputControllerOutlined?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        initUIView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func initUIView() {
        forgotUB.roundCorner(radius: 7.0)
        mdcEmailController = MDCTextInputControllerOutlined(textInput: emailMDCTF)
        mdcEmailController?.setMDCTextInputEmail()
        emailMDCTF.clearButton.isHidden = true
        nameBtn = UIButton(type: .custom)
        nameBtn.setImage(UIImage(named: "ic_mail"), for: .normal)
        nameBtn.tintColor = UIColor(named: "colorMainBlue")
        nameBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        nameBtn.frame = CGRect(x: CGFloat(emailMDCTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        emailMDCTF.rightView = nameBtn
        emailMDCTF.rightViewMode = .always
        emailMDCTF.delegate = self
        
        let tapMainUV = UITapGestureRecognizer(target: self, action: #selector(onClickMainUV))
        bgUIMG.addGestureRecognizer(tapMainUV)
    }
    
    @IBAction func onClickBackItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickForgotUB(_ sender: Any) {
       
        Auth.auth().sendPasswordReset(withEmail: emailMDCTF.text!, completion: {error in
            if error != nil{
                self.view.makeToast("Your Email is not authenticated.")
            } else{
                self.view.makeToast("Check your mail box.")
            }
        })
        
    }
    
    
    @objc func onClickMainUV() {
        emailMDCTF.resignFirstResponder()
        self.mdcEmailController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.mdcEmailController?.setErrorText("Match example@gmail.com", errorAccessibilityValue: nil)
    }
}
