//
//  CommentViewController.swift
//  TourLao
//
//  Created by Aira on 1/26/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController {

    @IBOutlet weak var commentTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        commentTV.rowHeight = UITableView.automaticDimension
        commentTV.estimatedRowHeight = 120.0
        commentTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        commentTV.dataSource = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onClickBackItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CommentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global.gComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = commentTV.dequeueReusableCell(withIdentifier: "CommentItem", for: indexPath) as! CommentItem
        cell.ratingCV.rating = Double(Global.gComment[indexPath.row].rate)
        cell.dateUL.text = Global.gComment[indexPath.row].date
        cell.contentUL.text = Global.gComment[indexPath.row].content
        
        for user in Global.gUserArr {
            if user.id == Global.gComment[indexPath.row].userid {
                Global.loadImage(url: user.photo, imageView: cell.avatarUIMG)
                cell.nameUL.text = user.name
                cell.countryUL.text = user.country
            }
        }
        return cell
    }
    
    
}
