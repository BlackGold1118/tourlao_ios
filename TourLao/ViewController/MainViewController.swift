//
//  MainViewController.swift
//  TourLao
//
//  Created by Aira on 1/21/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import MapKit
import LIHImageSlider
import OpenWeatherMapKit

class MainViewController: UIViewController {

    @IBOutlet weak var animationUV: UIView!
    @IBOutlet weak var weatherTxtUV: UIView!
    @IBOutlet weak var weatherContentUV: UIView!
    @IBOutlet weak var mapTxtUV: UIView!
    @IBOutlet weak var mapUV: MKMapView!
    @IBOutlet weak var etlUIMG: UIView!
    @IBOutlet weak var plazaUIMG: UIView!
    @IBOutlet weak var unitelUIMG: UIView!
    @IBOutlet weak var weathreUV: UIView!
    @IBOutlet weak var mapParentUV: UIView!
    @IBOutlet weak var todayTemp: UILabel!
    @IBOutlet weak var tomorrowTemp: UILabel!
    @IBOutlet weak var bannerScrollUV: UIScrollView!
    
    var search = UISearchController()
    var yOffSet: CGFloat = 0.0
    fileprivate var sliderVc1: LIHSliderViewController!
    
    var direction = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        initNavigationBar()
        initAnimationView()
        initUIView()
        initWeatherData()
        initBannerView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "main_tableView" {
            let destinationVC = segue.destination as! ListViewController
            destinationVC.category = sender as? String ?? ""
        }        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.sliderVc1!.view.frame = self.animationUV.frame
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initNavigationBar() {
        let rightNavUB = UIImage(named: "ic_profile")?.withRenderingMode(.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: rightNavUB, style: .plain, target: self, action: #selector(onTapProfile))
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.title = "Tour Laos Today"
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationItem.hidesBackButton = true
    }
    
    func initAnimationView() {
        let images: [UIImage] = [UIImage(named: "img_login_bg")!,UIImage(named: "img_login_bg")!,UIImage(named: "img_login_bg")!,UIImage(named: "img_login_bg")!]
        
        
        let slider1: LIHSlider = LIHSlider(images: images)
        slider1.sliderDescriptions = ["Image 1 description","Image 2 description","Image 3 description","Image 4 description"]
        self.sliderVc1  = LIHSliderViewController(slider: slider1)
//        sliderVc1.delegate = self
        self.addChild(self.sliderVc1)
        self.animationUV.addSubview(self.sliderVc1.view)
        self.sliderVc1.didMove(toParent: self)
    }
    
    func initWeatherData() {
        OpenWeatherMapKit.initialize(withAppId: "95c6186ba13a4acaf0ac7da84126c963")
        
        OpenWeatherMapKit.instance.weatherForecastForFiveDays(forCity: "Vientiane", withCountryCode: "la") { (forecast, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.todayTemp.text = "\(Int(Double((forecast?.list[0].main.celsius.currentTemp)!).truncate(places: 0)))" + " \u{2103}"
                    self.tomorrowTemp.text = "\(Int(Double((forecast?.list[8].main.celsius.currentTemp)!).truncate(places: 0)))" + " \u{2103}"
                }
            }
        }
        
    }
    
    func initBannerView() {
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.animateScrollView), userInfo: nil, repeats: true)
    }
    
    func initUIView() {        
        weathreUV.roundCorner(radius: 7.0)
        mapParentUV.roundCorner(radius: 7.0)
        
        etlUIMG.isUserInteractionEnabled = true
        let tapEtlUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapEtlUIMG))
        
        etlUIMG.addGestureRecognizer(tapEtlUIMG)
        
        plazaUIMG.isUserInteractionEnabled = true
        let tapPlazaUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapPlazaUIMG))
        
        plazaUIMG.addGestureRecognizer(tapPlazaUIMG)
        
        
        unitelUIMG.isUserInteractionEnabled = true
        let tapUnitelUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapUnitelUIMG))
        
        unitelUIMG.addGestureRecognizer(tapUnitelUIMG)
    }
    
    @objc func animateScrollView () {
        let currentY = self.etlUIMG.frame.origin.y
        if currentY == -120.0 {
            direction = 1
        }
        if currentY == 0.0 {
            direction = -1
        }
        goToPoint(direction: direction)
    }
    
    func goToPoint(direction: Int) {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.etlUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + CGFloat(direction) * 60.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
            self.plazaUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + 60.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
            self.unitelUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + 120.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
          }, completion: nil)
      }
    }
    
    @objc func onTapProfile() {
        self.performSegue(withIdentifier: "main_profile", sender: nil)
    }
    
    @objc func onTapEtlUIMG() {
        UIApplication.shared.openURL(NSURL(string: Global.urlEtl)! as URL)
    }

    @objc func onTapPlazaUIMG() {
        UIApplication.shared.openURL(NSURL(string: Global.urlPlaza)! as URL)
    }
    
    @objc func onTapUnitelUIMG() {
        UIApplication.shared.openURL(NSURL(string: Global.urlUnitel)! as URL)
    }
    
    @IBAction func onClickAttraction(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        self.performSegue(withIdentifier: "main_tableView", sender: String(button.tag))
    }    
}
