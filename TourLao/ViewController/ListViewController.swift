//
//  ListViewController.swift
//  TourLao
//
//  Created by Aira on 1/24/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import FirebaseDatabase
import CodableFirebase
import JMDropMenu

class ListViewController: UIViewController {

    @IBOutlet weak var listUTV: UITableView!
    @IBOutlet weak var etlUIMG: UIView!
    @IBOutlet weak var plazaUIMG: UIView!
    @IBOutlet weak var unitelUIMG: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var menuUV: UIView!
    @IBOutlet weak var bannerScrollUV: UIScrollView!
    
    var productArr = [Product]()
    var searchArr = [Product]()

    var category = ""
    
    var direction = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        initNavigationTitle()
        initData()
        initUIView()
        initBannerVIew()
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableView_detail" {
            let destinationVC = segue.destination as! DetailViewController
            destinationVC.product = sender as? Product
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func initNavigationTitle() {
        switch category {
        case "0":
            self.navigationItem.title = "Hotel / GuestHouse"
            break
        case "1":
            self.navigationItem.title = "Hospital"
            break
        case "2":
            self.navigationItem.title = "Entertainment / Night Life"
            break
        case "3":
            self.navigationItem.title = "Shopping"
            break
        case "4":
            self.navigationItem.title = "Transport / Bus / Airport"
            break
        case "5":
            self.navigationItem.title = "Attractions"
            break
        case "6":
            self.navigationItem.title = "Tour Operator"
            break
        default:
            print("none")
            break
        }
    }
    
    func initData(){
        Database.database().reference().child("Products").observe(.value, with: {snapshot in
            self.productArr.removeAll()
            for product in snapshot.children {
                let snap = product as! DataSnapshot
                guard let value = snap.value else { return }
                do {
                    let model = try FirebaseDecoder().decode(Product.self, from: value)
                    if model.category == self.category {
                        self.productArr.append(model)
                    }
                } catch let error {
                    print(error)
                }
            }

            self.searchArr = self.productArr
            self.listUTV.reloadData()
        })
    }
    
    func initUIView() {
        listUTV.rowHeight = UITableView.automaticDimension
        listUTV.estimatedRowHeight = 120.0
        listUTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        listUTV.dataSource = self
        searchBar.delegate = self
        
        etlUIMG.isUserInteractionEnabled = true
        let tapEtlUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapEtlUIMG))
        
        etlUIMG.addGestureRecognizer(tapEtlUIMG)
        
        plazaUIMG.isUserInteractionEnabled = true
        let tapPlazaUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapPlazaUIMG))
        
        plazaUIMG.addGestureRecognizer(tapPlazaUIMG)
        
        
        unitelUIMG.isUserInteractionEnabled = true
        let tapUnitelUIMG = UITapGestureRecognizer(target: self, action: #selector(onTapUnitelUIMG))
        
        unitelUIMG.addGestureRecognizer(tapUnitelUIMG)
    }
    
    func initBannerVIew() {
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.animateScrollView), userInfo: nil, repeats: true)
    }
    
    @objc func animateScrollView () {
        let currentY = self.etlUIMG.frame.origin.y
        if currentY == -120.0 {
            direction = 1
        }
        if currentY == 0.0 {
            direction = -1
        }
        goToPoint(direction: direction)
    }
    
    func goToPoint(direction: Int) {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.etlUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + CGFloat(direction) * 60.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
            self.plazaUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + 60.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
            self.unitelUIMG.frame = CGRect(x: 0.0, y: self.etlUIMG.frame.origin.y + 120.0, width: self.etlUIMG.frame.width, height: self.etlUIMG.frame.height)
          }, completion: nil)
      }
    }

    @IBAction func onClickBackItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickMenuItem(_ sender: Any) {
        let titleArr = ["Name","Date","Location"]
        let imageArr = ["ic_sort_alpha","ic_sort_date","ic_sort_location"]
        JMDropMenu.init(frame: CGRect(x:self.view.frame.width - 128, y: 64, width: 160, height: 125), arrowOffset: 102.0, titleArr: titleArr, imageArr: imageArr, type: .QQ, layoutType: .normal, rowHeight: 40.0, delegate: self)
    }
    
    @objc func onTapEtlUIMG() {
           UIApplication.shared.openURL(NSURL(string: Global.urlEtl)! as URL)
       }

       @objc func onTapPlazaUIMG() {
           UIApplication.shared.openURL(NSURL(string: Global.urlPlaza)! as URL)
       }
       
       @objc func onTapUnitelUIMG() {
           UIApplication.shared.openURL(NSURL(string: Global.urlUnitel)! as URL)
       }
    
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listUTV.dequeueReusableCell(withIdentifier: "ListItem", for: indexPath) as! ListItem
        
        let product = searchArr[indexPath.row]
        cell.initWithData(product: product)
        cell.delegate = self
        
        return cell
    }
}

extension ListViewController: ListItemDelegate {
    func onClickShareDelegate(product: Product) {
        //
    }
    
    func onClickContentUVDelegate(product: Product) {
        self.performSegue(withIdentifier: "tableView_detail", sender: product)
    }
    
    func onClickCommentDelegate(product: Product) {
        Database.database().reference().child("Comments").queryOrdered(byChild: "date").observe(.value, with: {snapshot in
            Global.gComment.removeAll()
            for comment in snapshot.children {
                let snap = comment as! DataSnapshot
                guard let value = snap.value else {
                    return
                }
                do {
                    let model = try FirebaseDecoder().decode(Comment.self, from: value)
                    if model.postid == product.id {
                        Global.gComment.append(model)
                    }
                } catch let error {
                    print(error)
                }
            }
            Global.gComment.reverse()
        })
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchArr = productArr
        } else {
            searchArr.removeAll()
            for item in productArr {
                if item.searchable(strKey: searchText){
                    searchArr.append(item)
                }
            }
        }
        self.listUTV.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ListViewController: JMDropMenuDelegate {
    
    func didSelectRow(at index: Int, title: String!, image: String!) {
        print(index)
        switch index {
        case 0:
            self.searchArr = self.searchArr.sorted(by: {$0.name > $1.name})
            break
        case 1:
            self.searchArr = self.searchArr.sorted(by: {$0.price > $1.price})
            break
        case 2:
            self.searchArr = self.searchArr.sorted(by: {$0.province > $1.province})
            break
        default:
            print("selected none")
            break
        }
        self.listUTV.reloadData()
    }
    
}
