//
//  ViewController.swift
//  TourLao
//
//  Created by Aira on 1/20/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import MaterialComponents
import Toast_Swift
import FirebaseAuth
import FirebaseDatabase
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController {

    @IBOutlet weak var mdcEmailTF: MDCTextField!
    @IBOutlet weak var mdcPasswordTF: MDCTextField!
    @IBOutlet weak var signInUB: UIButton!
    @IBOutlet weak var facebookUB: UIButton!
    @IBOutlet weak var signStackUV: UIStackView!
    @IBOutlet weak var mainUV: UIView!
    @IBOutlet weak var checkRememberUIMG: UIImageView!
    
    var mdcEmailController: MDCTextInputControllerOutlined?
    var mdcPasswordController: MDCTextInputControllerOutlined?
    var nameBtn: UIButton!
    var passwordToggleBtn : UIButton!
    var passwordToggle = false
    var isUp = false
    let valueUp: CGFloat = 100.0
    var isCheckRemember = false
    
    let loginManager:LoginManager = LoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        initUIView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initUIView() {
        if UserDefaults.standard.bool(forKey: "isRemeber") {
            isCheckRemember = true
            checkRememberUIMG.image = UIImage(named: "ic_check")
            mdcEmailTF.text = UserDefaults.standard.string(forKey: "email")
            mdcPasswordTF.text = UserDefaults.standard.string(forKey: "password")
        }
        signInUB.roundCorner(radius: 7.0)
        facebookUB.roundCorner(radius: 7.0)
        
        mdcEmailController = MDCTextInputControllerOutlined(textInput: mdcEmailTF)
        mdcEmailController?.setMDCTextInputEmail()
        mdcPasswordController = MDCTextInputControllerOutlined(textInput: mdcPasswordTF)
        mdcPasswordController?.setMDCTextInputEmail()
        
        mdcEmailTF.clearButton.isHidden = true
        mdcPasswordTF.clearButton.isHidden = true
        
        nameBtn = UIButton(type: .custom)
        nameBtn.setImage(UIImage(named: "ic_mail"), for: .normal)
        nameBtn.tintColor = UIColor(named: "colorMainBlue")
        nameBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        nameBtn.frame = CGRect(x: CGFloat(mdcEmailTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        mdcEmailTF.rightView = nameBtn
        mdcEmailTF.rightViewMode = .always
        
        passwordToggleBtn = UIButton(type: .custom)
        passwordToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
        passwordToggleBtn.tintColor = UIColor(named: "colorMainBlue")
        passwordToggleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        mdcPasswordTF.frame = CGRect(x: CGFloat(mdcPasswordTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(28), height: CGFloat(28))
        passwordToggleBtn.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        mdcPasswordTF.rightView = passwordToggleBtn
        mdcPasswordTF.rightViewMode = .always
        
        mdcEmailTF.delegate = self
        mdcPasswordTF.delegate = self
        
        let tapMainUV = UITapGestureRecognizer(target: self, action: #selector(onClickMainUV))
        mainUV.addGestureRecognizer(tapMainUV)
        
        checkRememberUIMG.isUserInteractionEnabled = true
        let tapCheck = UITapGestureRecognizer(target: self, action: #selector(onTapCheck))
        checkRememberUIMG.addGestureRecognizer(tapCheck)
    }
    
    @objc func refresh(){
        if !passwordToggle {
            passwordToggleBtn.setImage(UIImage(named: "ic_visible"), for: .normal)
            mdcPasswordTF.isSecureTextEntry = false
        } else {
            passwordToggleBtn.setImage(UIImage(named: "ic_invisible"), for: .normal)
            mdcPasswordTF.isSecureTextEntry = true
        }
        passwordToggle = !passwordToggle
        if isUp {
            Global.onAnimationUIView(view: self.signStackUV, moveSpace: valueUp)
            isUp = !isUp
        }
        mdcPasswordTF.resignFirstResponder()
    }
    
    @objc func onClickMainUV() {
        if isUp {
            Global.onAnimationUIView(view: self.signStackUV, moveSpace: valueUp)
            isUp = false
        }
        mdcPasswordTF.resignFirstResponder()
        mdcEmailTF.resignFirstResponder()
        self.mdcEmailController?.setErrorText(nil, errorAccessibilityValue: nil)
        self.mdcPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    @objc func onTapCheck() {
        if !isCheckRemember {
            checkRememberUIMG.image = UIImage(named: "ic_check")
        } else {
            checkRememberUIMG.image = UIImage(named: "ic_uncheck")
        }
        isCheckRemember = !isCheckRemember
    }
    
    @IBAction func onClickSignInUB(_ sender: Any) {
        if Global.emailValidation(email: mdcEmailTF.text!) && Global.passwordValidation(password: mdcPasswordTF.text!){
            // Firebase Sign In
            Global.onShowProgressView(name: "Loading...")
            Auth.auth().signIn(withEmail: mdcEmailTF.text!, password: mdcPasswordTF.text!, completion: {(user, error) in
                if error != nil {
                    Global.onhideProgressView()
                    self.view.makeToast("Account not exists.")
                } else{
                    Global.onhideProgressView()
                    if (user?.user.isEmailVerified)! {
                        Database.database().reference().child("Users").child((user?.user.uid)!).observe(.value, with: {snapshot in
                            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                            Global.gUser.fromFirebase(data: postDict)
                        })
                        UserDefaults.standard.set("Normal", forKey: "type")
                        self.performSegue(withIdentifier: "login_main", sender: nil)
                    } else {
                        self.view.makeToast("Check your mail box for verification.")
                    }
                }
            })
        }
        else {
            self.view.makeToast("No match Email or Password Validation")
        }
        if isCheckRemember {
            UserDefaults.standard.setValue(true, forKey: "isRemeber")
            UserDefaults.standard.setValue(mdcEmailTF.text, forKey: "email")
            UserDefaults.standard.setValue(mdcPasswordTF.text, forKey: "password")
        } else {
            UserDefaults.standard.removeObject(forKey: "isRemeber")
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject( forKey: "password")
        }
        
        getAllUserFB()
    }
    
    @IBAction func onClickFacebookUB(_ sender: Any) {
        loginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                    }

                }
            }
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
              if (error == nil){
                //everything works print the user data
                print(result as Any)
                guard let accessToken = AccessToken.current else {
                  print("Failed to get access token")
                  return
                }
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
                Global.onShowProgressView(name: "Loading...")
                Auth.auth().signIn(with: credential, completion: {(user, error) in
                    if error != nil {
                        self.view.makeToast("Error has been occured.")
                        Global.onhideProgressView()
                    } else {
                        Database.database().reference().child("Users").observe(.value, with: {snapshot in
                            if snapshot.hasChild(accessToken.userID){
                                self.getFBUserInfo(userId: accessToken.userID)
                            } else {
                                self.saveFBUSERInfo(result: result as! [String: Any], userID: accessToken.userID)
                            }
                        })
                        UserDefaults.standard.set("FaceBook", forKey: "type")
                        Global.onhideProgressView()
                        self.getAllUserFB()
                        self.performSegue(withIdentifier: "login_main", sender: nil)
                    }
                    
                })
              }
              else {
                print(error?.localizedDescription as Any)
                
              }
            })
      }
    }
    
    func getFBUserInfo(userId: String) {
        Database.database().reference().child("Users").child(userId).observe(.value, with: {snapshot in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            Global.gUser.fromFirebase(data: postDict)
        })
    }
    
    func saveFBUSERInfo(result: [String: Any], userID: String) {
        Global.gUser.id = userID
        Global.gUser.name = result["name"] as! String
        Global.gUser.email = ""
        Global.gUser.country = ""
        Global.gUser.type = 0
        
        if let picture = result["picture"] as? [String:Any] , let imgData = picture["data"] as? [String:Any] , let imgUrl = imgData["url"] as? String {
            Global.gUser.photo = imgUrl
        }
        Database.database().reference().child("Users").child(userID).setValue(Global.gUser.toFirebaseData())
    }
    
    func getAllUserFB() {
        Database.database().reference().child("Users").observe(.value, with: {snapshot in
            Global.gUserArr.removeAll()
            for user in snapshot.children {
                let memberUser: User = User()
                let snap = user as! DataSnapshot
                let userDict = snap.value as? [String: AnyObject] ?? [:]
                memberUser.fromFirebase(data: userDict)
                Global.gUserArr.append(memberUser)
            }
        })
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == mdcPasswordTF {
            self.mdcPasswordController?.setErrorText(nil, errorAccessibilityValue: nil)
            Global.onAnimationUIView(view: self.signStackUV, moveSpace: valueUp)
            isUp = false
        }
        if textField == mdcEmailTF {
            self.mdcEmailController?.setErrorText(nil, errorAccessibilityValue: nil)
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mdcPasswordTF {
            Global.onAnimationUIView(view: self.signStackUV, moveSpace: -valueUp)
            isUp = true
            self.mdcPasswordController?.setErrorText("Length must be over 6 digits.", errorAccessibilityValue: nil)
        }
        if textField == mdcEmailTF && isUp == true {
            Global.onAnimationUIView(view: self.signStackUV, moveSpace: valueUp)
            isUp = false
        }
        if textField == mdcEmailTF {
            self.mdcEmailController?.setErrorText("Match example@gmail.com", errorAccessibilityValue: nil)
        }
    }
}



