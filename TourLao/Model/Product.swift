//
//  Product.swift
//  TourLao
//
//  Created by Aira on 1/23/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation

struct Product: Codable {
    var id: String
    var photo: [String]
    var name: String
    var name_la: String
    var email: String
    var phone1: String
    var phone2: String
    var detail: String
    var detail_la: String
    var category: String
    var province: String
    var price: String
    var lat: Float
    var log: Float
    
    func searchable (strKey: String) -> Bool {
        if name.lowercased().contains(strKey.lowercased()) {
            return true
        }
        if price.contains(strKey) {
            return true
        }
        if Global.gProvince[Int(province)!].name.contains(strKey) {
            return true
        }
        return false
    }
}
