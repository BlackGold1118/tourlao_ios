//
//  Province.swift
//  TourLao
//
//  Created by Aira on 1/23/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation

class Province {
    var id: String = ""
    var name: String = ""
    var description: String = ""
    var img: Int = 0
    var lat: Double = 0.0
    var log: Double = 0.0
    
    init(id: String, name: String, description: String, img: Int, lat: Double, log: Double) {
        self.id = id
        self.name = name
        self.description = description
        self.img = img
        self.lat = lat
        self.log = log
    }
}
