//
//  User.swift
//  TourLao
//
//  Created by Aira on 1/22/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation

class User {
    var id: String
    var name: String
    var email: String
    var photo: String
    var country: String
    var type: Int
    
    init() {
        id = ""
        name = ""
        email = ""
        photo = ""
        country = ""
        type = 0
    }
    
    func toFirebaseData() -> [String: Any] {
        return [
            "id" : id,
            "name" : name,
            "email" : email,
            "photo" : photo,
            "country": country,
            "type": type
        ]
    }
    
    func fromFirebase(data: [String: Any]) {
        id = data["id"] as! String
        name = data["name"] as! String
        email = data["email"] as! String
        photo = data["photo"] as! String
        country = data["country"] as! String
        type = data["type"] as! Int
    }
}
