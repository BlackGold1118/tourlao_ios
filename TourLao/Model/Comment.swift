//
//  Comment.swift
//  TourLao
//
//  Created by Aira on 1/25/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import Foundation
struct Comment: Codable {
    var id: String
    var commentid: String
    var postid: String
    var userid: String
    var content: String
    var date: String = ""
    var rate: Int
}
