//
//  CommentItem.swift
//  TourLao
//
//  Created by Aira on 1/26/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import Cosmos

class CommentItem: UITableViewCell {
    
    @IBOutlet weak var avatarUIMG: UIImageView!
    @IBOutlet weak var ratingCV: CosmosView!
    @IBOutlet weak var nameUL: UILabel!
    @IBOutlet weak var countryUL: UILabel!
    @IBOutlet weak var dateUL: UILabel!
    @IBOutlet weak var contentUL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarUIMG.roundCorner(radius: avatarUIMG.frame.size.height / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
