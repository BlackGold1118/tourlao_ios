//
//  ListItem.swift
//  TourLao
//
//  Created by Aira on 1/24/20.
//  Copyright © 2020 Laodev. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import CodableFirebase

protocol ListItemDelegate {
    func onClickShareDelegate(product: Product)
    func onClickCommentDelegate(product: Product)
    func onClickContentUVDelegate(product: Product)
}

class ListItem: UITableViewCell {

    @IBOutlet weak var tourUIMG: UIImageView!
    @IBOutlet weak var tourTitleUL: UILabel!
    @IBOutlet weak var tourDescUL: UILabel!
    @IBOutlet weak var likeCountUL: UILabel!
    @IBOutlet weak var commentCountUL: UILabel!
    @IBOutlet weak var contentUV: UIView!
    @IBOutlet weak var likeUB: UIButton!
    
    var delegate: ListItemDelegate?
    var product: Product?
    var likeArr: [String] = [String]()
    var commentArr: [Comment] = [Comment]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        let tapContentUV = UITapGestureRecognizer(target: self, action: #selector(onTapContentUV))
        contentUV.addGestureRecognizer(tapContentUV)
    }
    
    func initWithData(product: Product) {
        self.product = product
        
        self.tourTitleUL.text = product.name
        self.tourDescUL.text = product.detail
        Global.loadImage(url: product.photo[0], imageView: self.tourUIMG)
        
        initCellData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCellData() {
        Database.database().reference().child("Likes").observe(.value, with: {snapshot in
            self.likeArr.removeAll()
            if snapshot.hasChild(self.product!.id){
                self.likeCountUL.text = "\(snapshot.childSnapshot(forPath: self.product!.id).childrenCount)"
                for ownLike in snapshot.childSnapshot(forPath: self.product!.id).children{
                    let like = ownLike as! DataSnapshot
                    self.likeArr.append((like.value as? String)!)
                }
                if self.likeArr.contains(Global.gUser.id) {
                    self.likeUB.setImage(UIImage(named: "ic_like_blue"), for: .normal)
                } else {
                    self.likeUB.setImage(UIImage(named: "ic_like"), for: .normal)
                }
            } else {
                self.likeUB.setImage(UIImage(named: "ic_like"), for: .normal)
                self.likeCountUL.text = "0"
            }
        })
        
        Database.database().reference().child("Comments").queryOrdered(byChild: "date").observe(.value, with: {snapshot in
            self.commentArr.removeAll()
            for comment in snapshot.children {
                let snap = comment as! DataSnapshot
                guard let value = snap.value else {
                    return
                }
                do {
                    let model = try FirebaseDecoder().decode(Comment.self, from: value)
                    if model.postid == self.product!.id {
                        self.commentArr.append(model)
                    }
                } catch let error {
                    print(error)
                }
            }
            self.commentArr.reverse()
            self.commentCountUL.text = "\(self.commentArr.count)"
        })
    }

    @IBAction func onClickShareUB(_ sender: Any) {
        delegate!.onClickShareDelegate(product: product!)
    }
    
    @IBAction func onClickCommentUB(_ sender: Any) {
        delegate!.onClickCommentDelegate(product: self.product!)
    }
    
    @IBAction func onClickLikeUB(_ sender: Any) {
        if likeUB.currentImage == UIImage(named: "ic_like") {
            likeArr.append(Global.gUser.id)
            Database.database().reference().child("Likes").child(self.product!.id).setValue(self.likeArr)
        }
        else {
            if let idx = likeArr.firstIndex(of: Global.gUser.id) {
                likeArr.remove(at: idx)
            }
            Database.database().reference().child("Likes").child(self.product!.id).setValue(self.likeArr)
        }
    }
    
    @objc func onTapContentUV() {
        delegate!.onClickContentUVDelegate(product: self.product!)
    }
}
